from enum import Enum
from django.db import models
from core.models import TimeStampedModel


class Company(TimeStampedModel):
    class TYPES(Enum):
        unknown = ('un', 'Unknown')
        saas = ('ss', 'Subscription SaaS')
        news_ent = ('ne', 'News or Entertainment')
        social_media = ('sm', 'Social Media')
        gaming = ('gm', 'Gaming')
        ecommerce = ('ec', 'E-commerce')
        trans_fund = ('tf', 'Transactions or Funding')
        sharing_p2p = ('sp', 'Sharing Economy / Peer-to-Peer Service')
        ratings = ('rn', 'Ratings Network')
        enterprise = ('ep', 'Large Enterprise Platform')

        @classmethod
        def get_value(cls, member):
            return cls[member].value[0]

    name = models.CharField(max_length=200)
    anonymous = models.BooleanField(default=True)
    num_employees = models.PositiveIntegerField(default=1)
    type = models.CharField(
        max_length=2,
        choices=[x.value for x in TYPES],
        default='un'
    )

    def __str__(self):
        return self.name

    def get_anonymous_display(self):
        return 'A %s company with %d employees' % \
            (self.get_type_display(), self.num_employees)

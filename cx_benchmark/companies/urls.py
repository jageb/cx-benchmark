from django.conf.urls import url

from . import views

urlpatterns = [
    url(
        regex=r'^~update/$',
        view=views.CompanyUpdateView.as_view(),
        name='update'
    ),
]

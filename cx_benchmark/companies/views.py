# from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.views.generic import DetailView, ListView, RedirectView, UpdateView

from django.contrib.auth.mixins import LoginRequiredMixin

from companies.models import Company


class CompanyUpdateView(LoginRequiredMixin, UpdateView):

    fields = ['name', 'anonymous', 'num_employees', 'type']

    model = Company

    # send the user back to company page after a successful update
    def get_success_url(self):
        return reverse('users:detail',
                       kwargs={'username': self.request.user.username})

    def get_object(self):
        # Only get the Company record for the user making the request
        return Company.objects.get(id=self.request.user.company.id)

from enum import Enum
from django.db import models
from core.models import TimeStampedModel
from companies.models import Company


class InsightManager(models.Manager):
    def for_company(self, company):
        return self.filter(company=company) 


class Insight(TimeStampedModel):
    class TYPES(Enum):
        unspecified = ('us', 'Unspecified')
        business_type = ('bt', 'Business Type')

        @classmethod
        def get_value(cls, member):
            return cls[member].value[0]

    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    shared = models.BooleanField(default=False)
    type = models.CharField(
        max_length=2,
        choices=[x.value for x in TYPES],
        default=TYPES.get_value('unspecified'),
    )
    value = models.CharField(max_length=255)
    objects = InsightManager()

    def __str__(self):
        return 'Insight: %s (%s)' % (self.company.name, self.type)

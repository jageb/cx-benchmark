#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset


celery -A cx_benchmark.taskapp beat -l INFO
